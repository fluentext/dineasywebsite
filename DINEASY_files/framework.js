
jQuery(document).ready(function () {
    wow = new WOW(
{
    mobile: false
});
    wow.init();
});

var isTweetsLoaded = false;
var isITSliderInitialized = false;
var isMechanicalSliderInitialized = false;
var isElectiricalSliderInitialized = false;

//For Man made Slider
var imageNo = 1;
function showDiv() {
    jQuery('#progress').css("width", "0px");
    jQuery('#progress').stop();
    jQuery('#progress').hide();
    progress();

    imageNo++;
    
    imageNo = imageNo == 4 ? 1 : imageNo;

    jQuery('#bgDiv').fadeOut(900, function () {
        jQuery('#bgDiv img').attr('src', './Images/slides/Slide-' + imageNo + '.JPG?v=3.9');
        
        jQuery('#bgDiv img').attr('data-current', imageNo);
        jQuery('.pager-container span').removeClass('active');
        jQuery(".pager-container span[data-index='" + (imageNo - 1) + "']").addClass('active');
        jQuery('#bgDiv').fadeIn(600);
    });
    //alert("hi");
};


if (typeof isInnerPage != 'undefined' && !isInnerPage)
{
   // alert("hello")
var timer = setInterval(showDiv, 9000);
}

var progress = function () {
    jQuery('#progress').show();

    jQuery('#progress').animate({ width: "100%" }, 9000, 'linear', function () {
        //setTimeout(progress, (pause / limit) - (diff / limit));
        jQuery('#progress').css({ width: '0%' });
    });
    // } else {
    // $('#progress').css({ width: '0%' });
    // }
};

function encryptSubmissionsRandomString() {
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var length = 20;
    var randomstring = '';
    for (var i = 0; i < length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    return randomstring;
}


//This function prepares renders the tweet of html
function ctdatarendering(result) {
    jQuery('.loading').hide();
    jQuery('.tweetsContainer').show();
    jQuery.each(result, function (key, value) {
        var res = jQuery.parseJSON(value);
        jQuery.each(res, function (key, val) {

            var celbritytweet = "";
            celbritytweet += "<li><div class='celebrityeachtextdiv' id='celbritytweetseachdiv" + key + "' >";
            celbritytweet += "<div  id='mainheader" + key + "'  class='boxshadow celebrityeachdiv'><div id='clbheader' class='celebrityeachheaderdiv'></div>"
            celbritytweet += "<div class='celebrityeachtweet' id='text'" + key + " >" + validateUrl(val.text) + "</div>";
            celbritytweet += "<div class='celebritytweeteddate' id='date'" + key + " >@" + val.user.screen_name + " - " + val.created_at.split('+')[0] + "</div> </br>";
            celbritytweet += "</div></li>";
            jQuery(".twitter-bxslider").append(celbritytweet);
        });

    });

    tweetCrouselInit();
}
//This method gets the user tweets from server
function getTweets() {
    jQuery.ajax({
        url: "/api/ITInAndhraAPI/GetAllTweets?twitterHandle=AndhraPradeshCM",
        type: "GET",
        dataType: "json",
        success: function (data) {
            ctdatarendering(data);
        },
        error: function (x, y) {
            jQuery('#accordian-tweets').attr('data-isfirsttime', false);
        }
    });
}

//This function looks for url in text and replace it with anchor
function validateUrl(text) {
    // var str = "The rain in SPAIN stays mainly in the plain http://www.kamedy.com sdfgf gfdsg dsfsdf http://www.crowdblood.org";
    var res = text.match(/(https?:\/\/(([-\w\.]+)+(:\d+)?(\/([\w/_\.]*(\?\S+)?)?)?))/g);
    var str = text;
    if (null != res) {
        jQuery.each(res, function (key, val) {
            str = text.replace(val, "<a href='" + val + "' class='tweetLinks' target='_blank'>" + val + "</a>");
        });
    }

    //validate #tags
    var res1 = str.match(/#([^ ]+)/);
    var str1 = str;
    if (null != res1) {
        jQuery.each(res1, function (key, val) {
            str1 = str.replace('#' + val, "<span class='tweetLinks'><b>#" + val + "</b></span>");
        });
    }

    return str1;
}

function tweetCrouselInit() {
    twitterslider = jQuery('.twitter-bxslider').bxSlider({
        auto: true,
        pager: true,
        autoHover: true,
    });
}

var jhomeSlogan = [];
var jhomeAdvantages = [];
var winHeight = window.innerHeight + 100;
var actualWinWidth = window.innerWidth;
var bodyWidth = 1024;
var homeAdvantagesMobile = 639;
var isFromApp = false;

var graphDataObj = [{ "fy20162017": [0, 0, 0, 0, 0] }, { "fy20172018": [0, 0, 0, 0, 0] }, { "fy20182019": [0, 0, 0, 0, 0] }, { "fy20192020": [0, 0, 0, 0, 0] }, { "fy20202020": [0, 0, 0, 0, 0] }];

var heightSlider = 0;
try {
    heightSlider = document.getElementById("slider-div").clientHeight + 80;
} catch (e) {

}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

window.onload = function () {
    try {

        jQuery(".turnover-forecast,.emp-gen").blur();

        // For hiding header and footer
        var queryParam = getParameterByName("t");
        if (queryParam == 'mobile') {
            jQuery('header').addClass('dont-show');
            jQuery('footer').addClass('dont-show');
            jQuery('.body-ja-container').addClass('appview');
            jQuery('#home-logos.fixed').addClass('appview');
            isFromApp = true;
        }

            jQuery("#slider-div").css("opacity", "1");

            var currentUrl = window.location.href;

            var containerName = currentUrl.split("#")[1];

            jQuery(".slide-up-down[data-containername='" + containerName + "'] > h2").removeClass("active");
            jQuery(".slide-updown-container[data-containername='" + containerName + "']").removeClass("active");
            jQuery('html, body').animate({
                scrollTop: jQuery(".slide-updown-container[data-containername='" + containerName + "']").offset().top + heightSlider - 250
            }, 'slow');
    } catch (e) {
        console.log(e);
    }


    try {
        var currentUrl = window.location.href;
        if (currentUrl.indexOf("mHome") >= 0) {
            var blockId = jQuery("#" + currentUrl.split("#")[1]).attr("data-blockid");
            jQuery(".calculate-benefits").removeClass("active");
            jQuery(".calculate-benefits[data-blockid='" + blockId + "']").addClass("active");
        } else if (currentUrl.indexOf("#benefitscalculator") >= 0 || currentUrl.indexOf("#benefitsstartup") >= 0 || currentUrl.indexOf("#benefitssme") >= 0 || currentUrl.indexOf("#benefitsmega") >= 0) {
            jQuery("#tab-benefits > ul > li").removeClass("selected");
            jQuery("#" + currentUrl.split("#")[1]).addClass("selected");
            var blockId = jQuery("#" + currentUrl.split("#")[1]).attr("data-blockid");
            jQuery(".calculate-benefits").removeClass("active");
            jQuery(".calculate-benefits[data-blockid='" + blockId + "']").addClass("active");
            jQuery("html,body").animate({
                scrollTop: jQuery("#" + currentUrl.split("#")[1]).offset().top - heightSlider / 2
            }, 'slow');
        }
    } catch (e) {
    }
}

jQuery(document).ready(function () {


    try {
        google.charts.load('current', { 'packages': ['corechart'] });

    } catch (e) {

    }

    jQuery(".profilePicWH-right > ul > li").click(function (e) {
        e.preventDefault();
        rippleAnimation(jQuery(this), e);
        jQuery(".profilePicWH-right > ul > li").removeClass("active");
        jQuery(this).addClass("active");
        var id = jQuery(this).attr("data-tabid");
        if (!id) {
            return;
        }
        jQuery(".tab_content").removeClass("active");
        jQuery("#" + id).addClass("active");

        jQuery('html, body').animate({
            scrollTop: 0
        }, 'slow');

        //$("#hp-date-of-birth").datepicker({
        //    changeMonth: true,
        //    changeYear: true,
        //    maxDate: new Date(),
        //    yearRange: "1900:2012",
        //    dateFormat: 'mm/dd/yy'
        //});

    });

    // START :: Showing SignOut menu popup
    jQuery(".signout-button").live("click touchend", function (e) {
        e.preventDefault();
        // alert("clicked");
        jQuery(".user-details,.user-details-container .arrow-up-border,.user-details-container .arrow-up-background").toggleClass("active");
    });


    jQuery(".dash-enabler").click(function () {
        //alert("hi");
        var state = jQuery(this).attr("data-state");
        if (state == "on") {
            jQuery(this).attr("data-state", "off");
            jQuery("#menu-profile").addClass("collapsed");
            jQuery(".userDashboard-container").addClass("collapsed");
        }
        else {
            jQuery(this).attr("data-state", "on");
            jQuery("#menu-profile").removeClass("collapsed");
            jQuery(".userDashboard-container").removeClass("collapsed");
        }

    });

    //testimonial slider initialization
    jQuery("#it-client-slider").bxSlider({
    });

    // START :: Video overlay show/hide
    jQuery(".play-icon").click(function (e) {
        e.preventDefault();
        jQuery(".cb-video-overlay iframe").attr("src", "https://www.youtube.com/embed/JmduV515yHc?modestbranding=1&autohide=1&showinfo=0&autoplay=1");
        jQuery(".video-overlay-background,.cb-video-overlay").addClass("active");
    });
    jQuery(".cb-video-overlay-close").click(function () {
        jQuery(".video-overlay-background,.cb-video-overlay").removeClass("active");
        setTimeout(function () {
            jQuery(".cb-video-overlay iframe ").attr("src", "about:blank");
        }, 400);
    });
    // END :: Video overlay show/hide

    jQuery("#slider-div").css({ "position": "fixed", "top": "80px" });


    jQuery("#tab-benefits > ul > li").click(function () {
        jQuery("#tab-benefits > ul > li").removeClass("selected");
        jQuery(this).addClass("selected");
        var blockId = jQuery(this).attr("data-blockid");
        jQuery(".calculate-benefits").removeClass("active");
        jQuery(".calculate-benefits[data-blockid='" + blockId + "']").addClass("active");
    })

   // jQuery("#page-header").parallax({ imageSrc: 'http://html.templines.com/meshkov/oscend/img/sections/section-8.jpg', position: '50% -95px' });

    jQuery('.numbersOnly').keydown(function (event) {
        // Allow: backspace, delete, tab, escape, enter and .
        if (jQuery.inArray(event.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });

    jQuery(".turnover-forecast,.emp-gen").blur(function () {
        var obj = jQuery(this).parent().parent();
        var empGen = obj.find(".emp-gen").val().trim();
        if (!empGen) {
            empGen = 0;
        }
        var turnoverForecast = obj.find(".turnover-forecast").val().trim();
        if (!turnoverForecast) {
            turnoverForecast = 0;
        }

        var id = obj.attr("class");
        if ((empGen == 0 && turnoverForecast == 0)) {
            switch (id) {
                case "fy20162017":
                    graphDataObj[0].fy20162017 = [0, 0, 0, 0, 0];
                    break;
                case "fy20172018":
                    graphDataObj[1].fy20172018 = [0, 0, 0, 0, 0];
                    break;
                case "fy20182019":
                    graphDataObj[2].fy20182019 = [0, 0, 0, 0, 0];
                    break;
                case "fy20192020":
                    graphDataObj[3].fy20192020 = [0, 0, 0, 0, 0];
                    break;

                case "fy20202020":
                    graphDataObj[4].fy20202020 = [0, 0, 0, 0, 0];
                    break;
            }

            jQuery("#chart_div").css("display", "block");
            google.charts.setOnLoadCallback(drawVisualization);
            obj.find(".land-benefits,.tax-benefits,.emp-benefits,.xyz-benefits,.abc-benefits").html("-");
            return;
        }

        obj.find(".land-benefits").html((empGen + turnoverForecast) / 5);
        obj.find(".tax-benefits").html((empGen + turnoverForecast) / 4);
        obj.find(".emp-benefits").html((empGen * turnoverForecast) / 5);
        obj.find(".xyz-benefits").html((empGen - turnoverForecast) / 4);
        obj.find(".abc-benefits").html((turnoverForecast - empGen) / 2);

        switch (id) {
            case "fy20162017":
                graphDataObj[0].fy20162017 = [(empGen + turnoverForecast) / 5, (empGen + turnoverForecast) / 4, (empGen * turnoverForecast) / 5, (empGen - turnoverForecast) / 4, (turnoverForecast - empGen) / 2];
                break;
            case "fy20172018":
                graphDataObj[1].fy20172018 = [(empGen + turnoverForecast) / 5, (empGen + turnoverForecast) / 4, (empGen * turnoverForecast) / 5, (empGen - turnoverForecast) / 4, (turnoverForecast - empGen) / 2];
                break;
            case "fy20182019":
                graphDataObj[2].fy20182019 = [(empGen + turnoverForecast) / 5, (empGen + turnoverForecast) / 4, (empGen * turnoverForecast) / 5, (empGen - turnoverForecast) / 4, (turnoverForecast - empGen) / 2];
                break;
            case "fy20192020":
                graphDataObj[3].fy20192020 = [(empGen + turnoverForecast) / 5, (empGen + turnoverForecast) / 4, (empGen * turnoverForecast) / 5, (empGen - turnoverForecast) / 4, (turnoverForecast - empGen) / 2];
                break;
            case "fy20202020":
                graphDataObj[4].fy20202020 = [(empGen + turnoverForecast) / 5, (empGen + turnoverForecast) / 4, (empGen * turnoverForecast) / 5, (empGen - turnoverForecast) / 4, (turnoverForecast - empGen) / 2];
                break;
        }
        

        jQuery("#chart_div").css("display", "block");
        google.charts.setOnLoadCallback(drawVisualization);
    });

    jQuery(".slide-up-down > h2").click(function () {
        jQuery(this).toggleClass("active");
        var dataContainer = jQuery(this).attr("data-containername");
        jQuery(".slide-updown-container[data-containername='" + dataContainer + "']").toggleClass("active");
    });

    try {
            jQuery("html").niceScroll({
                cursorcolor: "#CC202E",
                cursorwidth: "15px", // cursor width in pixel (you can also write "5px")
                cursorborder: "0px solid #fff", // css definition for cursor border
                zindex: 9999999
            });
    } catch (e) {

    }

    jQuery(".scroll-down").click(function () {
        jQuery('html,body').animate({
            scrollTop: jQuery("#wrapper").offset().top - 80
        });
    });
    
    jQuery('.scroller img').hover(function () {
        var selectedCity = jQuery(this).attr('data-logo');
        
        jQuery('img[data-logo=' + selectedCity.toLocaleLowerCase() + ']').attr('src', '/Images/ITINAP/' + selectedCity + '-green.png');
    }, function () {
        var selectedCity = jQuery(this).attr('data-logo');
        if (!jQuery(this).hasClass('active')) {
            jQuery('img[data-logo=' + selectedCity.toLocaleLowerCase() + ']').attr('src', '/Images/ITINAP/' + selectedCity + '.png');

        }
    });

    jQuery(".cityparent").click(function () {
        jQuery(".city-details-container").addClass("active");
        var datarel = jQuery(this).attr("data-rel");
        jQuery(".tabsul li").removeClass("active");
        jQuery(".city-details").removeClass("active");
        jQuery("#" + datarel).addClass("active");
        jQuery('.tabsul li[data-rel=' + datarel + ']').addClass("active");

        jQuery("#example-graph-btn2 a .open").addClass("active");
        jQuery("#example-graph-btn2 a .close").addClass("active");
    });

    jhomeSlogan = jQuery('#home-why .slogan');
    jhomeAdvantages = jQuery('#home-why .advantages');
    jhomeFeatures = jQuery('#home-why .feature');

    initAdvantages();
    if (actualWinWidth <= 1024) {
        runAdvantages();
    }

    if (typeof legitPage != "undefined") {
        jQuery(".topbar,.article-left").addClass("active");
        var id1 = window.location.hash;
        setTimeout(function () {
            jQuery('html,body').animate({
                scrollTop: jQuery("" + id1).offset().top - 100
            },
        'slow');
            jQuery(".article-left ul li a").removeClass("selected");
            jQuery(".article-left ul li a[data-named='" + id1 + "']").addClass("selected");
        }, 200);

        jQuery(".footer ul li a").click(function () {
            jQuery(".article-left ul li a").removeClass("selected");
            var id = jQuery(this).attr("data-named");
            jQuery(".article-left ul li a[data-named='" + id + "']").addClass("selected")
            jQuery('html,body').animate({
                scrollTop: jQuery("" + id).offset().top - 100
            },
            'slow');
        });

        jQuery(".article-left ul li a").click(function () {
            jQuery(".article-left ul li a").removeClass("selected");
            jQuery(this).addClass("selected");

            var id = jQuery(this).attr("data-named");

            jQuery('html,body').animate({
                scrollTop: jQuery("" + id).offset().top - 100
            },
            'slow');
        });
    }


    if (null != document.getElementById('progress')) {
        progress();
    }

    try {
        if (!isInnerPage) {

            jQuery('#bgDiv').hover(function (ev) {
                clearInterval(timer);
            }, function (ev) {
                timer = setInterval(showDiv, 9000);
            });
        }
    } catch (e) {

    }

    jQuery('#btn-prev').click(function () {
        clearInterval(timer);
        var currentImgNo = jQuery('#bgDiv img').attr('data-current');
        if (currentImgNo == "1") {
            imageNo = 2;
        }
        else {
            imageNo = imageNo - 2;
        }
        showDiv();
    });

    jQuery("#btn-next").click(function () {
        clearInterval(timer);
        var currentImgNo = jQuery('#bgDiv img').attr('data-current');

        if (currentImgNo == "3") {
            imageNo = 0;
        }
        showDiv();
    });

    jQuery(".pager-container span").click(function () {
        var imgToShow = jQuery(this).attr('data-index');
        imageNo = imgToShow;
        showDiv();
    });


    jQuery('#backtotop').on("click", function () {
        jQuery('html,body').animate({ scrollTop: 0 }, 1000, function () {
            // alert("reached top");
        });
    });

    jQuery('#ja-contact-us, #menu-contact-ja').click(function (e) {
        e.preventDefault();
        jQuery('#cbp-spmenu-contact').addClass('cbp-spmenu-open');
    });

    jQuery("#ja-share-idea-with, #menu-share-ja, #footer-share-us").click(function (e) {
        e.preventDefault();
        jQuery('#cbp-spmenu-shareIdea').addClass('cbp-spmenu-open');
    });

    jQuery("#toggle-table-graph").click(function () {
        jQuery(".calculate-benefits").toggleClass("active");
        jQuery("#example-graph-btn1 a .open").toggleClass("active");
        jQuery("#example-graph-btn1 a .close").toggleClass("active");
    });

    jQuery("#toggle-table-history").click(function () {
        jQuery(".city-details-container").toggleClass("active");
        jQuery("#example-graph-btn2 a .open").toggleClass("active");
        jQuery("#example-graph-btn2 a .close").toggleClass("active");

        jQuery(".tabsul li").removeClass("active");
        jQuery(".city-details").removeClass("active");
        jQuery("#hd-history").addClass("active");
        jQuery('.tabsul li[data-rel="hd-history"]').addClass("active");


    });

});

jQuery(window).resize(function () {
    if (null != document.getElementById("slider-div")) {
        resizeParalx();
    }
    google.charts.setOnLoadCallback(drawVisualization);
});

// Validate an object for null, undefined and empty (means it has no properties of its own)
function isEmpty(obj) {
    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that property is correct.
    if (obj.length > 0) return false;
    if (obj.length === 0) return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

// Showing ripple animation upon mousedown
function rippleAnimation(parent, e) {
    //create .ink element if it doesn't exist
    if (parent.find(".ink").length == 0)
        parent.prepend("<span class='ink'></span>");

    ink = parent.find(".ink");
    //incase of quick double clicks stop the previous animation
    ink.removeClass("animateripple");

    //set size of .ink
    if (!ink.height() && !ink.width()) {
        //use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
        d = Math.max(parent.outerWidth(), parent.outerHeight());
        ink.css({ height: d, width: d });
    }

    //get click coordinates
    //logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
    x = e.pageX - parent.offset().left - ink.width() / 2;
    y = e.pageY - parent.offset().top - ink.height() / 2;

    //set the position and add class .animate
    ink.css({ top: y + 'px', left: x + 'px' }).addClass("animateripple");
}


// Add or update query parameter to browser url
function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        return uri + separator + key + "=" + value;
    }
}

function resizeParalx() {

    var height = jQuery(".slider-div").outerHeight() + 80;

    jQuery("#parallax-div").css("margin-top", height);
    jQuery("#slider-div").css({ "position": "fixed", "top": "80px" });
}

function drawVisualization() {
    // Some raw data (not necessarily accurate)
    var data = google.visualization.arrayToDataTable([
     ['Fiscal Year', 'Land Benefits', 'Tax Benefits', 'Employee Benefits', 'XYZ Benefits', 'ABC Benefits'],
     ['2016-2017', graphDataObj[0].fy20162017[0], graphDataObj[0].fy20162017[1], graphDataObj[0].fy20162017[2], graphDataObj[0].fy20162017[3], graphDataObj[0].fy20162017[4]],
     ['2017-2018', graphDataObj[1].fy20172018[0], graphDataObj[1].fy20172018[1], graphDataObj[1].fy20172018[2], graphDataObj[1].fy20172018[3], graphDataObj[1].fy20172018[4]],
     ['2018-2019', graphDataObj[2].fy20182019[0], graphDataObj[2].fy20182019[1], graphDataObj[2].fy20182019[2], graphDataObj[2].fy20182019[3], graphDataObj[2].fy20182019[4]],
    ['2019-2020', graphDataObj[3].fy20192020[0], graphDataObj[3].fy20192020[1], graphDataObj[3].fy20192020[2], graphDataObj[3].fy20192020[3], graphDataObj[3].fy20192020[4]],
    ['2020-2020', graphDataObj[4].fy20202020[0], graphDataObj[4].fy20202020[1], graphDataObj[4].fy20202020[2], graphDataObj[4].fy20202020[3], graphDataObj[4].fy20202020[4]]
    ]);

    var options = {
        title: 'benefits for your company',
        vAxis: { title: 'Benefits' },
        hAxis: { title: 'Fiscal Year' },
        seriesType: 'bars',
        series: { 5: { type: 'line' } }
    };

    var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
    chart.draw(data, options);
}

function initAdvantages() {
    if (jhomeAdvantages.length && !jhomeAdvantages.hasClass('completed')) {
        jhomeAdvantages.removeClass('animating');
        if (jhomeAdvantages.hasClass('desktop')) {
            jhomeAdvantages.find('.advantage').each(function () {
                jadvantage = jQuery(this);
                jadvantage.data('targetTop', jadvantage.css('top'));
            });
            jhomeAdvantages.addClass('animating');
        }
    }
}

function runAdvantages() {
    if (jhomeSlogan.length && jhomeAdvantages.length) {
        if (jhomeAdvantages.hasClass('animating') && !jhomeAdvantages.hasClass('completed')) {
            jhomeSlogan.addClass('completed').find('.gray').slideUp(1200);
            if (jhomeAdvantages.hasClass('desktop')) {
                jhomeAdvantages.find('.leaf .animate').animate({ width: '100%', height: '100%' }, 1200);
                jhomeAdvantages.find('.advantage').each(function () {
                    jadvantage = jQuery(this);
                    delay = Math.floor((Math.random() * 150) + 50);
                    duration = Math.floor((Math.random() * 1100) + 1000);
                    targetTop = jadvantage.data('targetTop');
                    jadvantage.delay(delay).animate({ top: targetTop }, duration);
                });
            }
        }
        jhomeAdvantages.addClass('completed');
    }
}

function resetAdvantages() {
    if (jhomeSlogan.length && jhomeAdvantages.length) {
        jhomeSlogan.find('.gray').slideDown(10, function () {
            jhomeSlogan.removeClass('completed');
            jhomeAdvantages.removeClass('completed');
        });
        jhomeAdvantages.find('.leaf .animate').animate({ width: '1%', height: '1%' }, 10, function () {
            jQuery(this).parents('.advantage').css('top', '');
        });
    }
}

function runFeatures(jfeatures) {
    if (!jfeatures.first().hasClass('completed')) {
        jfeatures.removeClass('reset').first().addClass('completed').find('.title .animate').animate({ width: '0%' }, 300, function () {
            if ((jfeatures.length > 1) && (!jfeatures.first().hasClass('reset'))) {
                runFeatures(jfeatures.slice(1));
            }
        });
    } else {
        if (jfeatures.length > 1) {
            runFeatures(jfeatures.slice(1));
        }
    }
}

function resetFeatures() {
    if (jhomeFeatures.length) {
        jhomeFeatures.addClass('reset').removeClass('completed').find('.title .animate').animate({ width: '100%' }, 10);
    }
}

function contactusRequest(e) {
    //e.preventdefault();

    var name = jQuery('#txt-name').val().trim();
    var companyname = jQuery('#txt-companyname').val().trim();
    var position = jQuery('#txt-position').val().trim();
    var emailid = jQuery('#txt-email').val().trim();
    var telephone = jQuery('#txt-phone').val().trim();
    var areaofintrest = jQuery('#txt-area-of-interest').val().trim();
    var query = jQuery('#txt-description').val().trim();

    var feedback = new Object();
    feedback.Name = name;
    feedback.Company = companyname;
    feedback.PositionInCompany = position;
    feedback.FeedBack = query;
    feedback.AreaOfInterest = areaofintrest;
    feedback.EmailAddress = emailid;
    feedback.PhoneNumber = telephone;

    feedbackCallExecute(feedback);

    return false;
}

function clearcontactform() {
    jQuery('#txt_firstName').val("");
    jQuery('#txt_emailAddress').val("");
    jQuery('#txt_telephone').val("");
    jQuery('#ddl_howFound').val("");
    jQuery('#txt_questions').val("");

    jQuery('#txt_firstName1').val("");
    jQuery('#txt_emailAddress1').val("");
    jQuery('#txt_telephone1').val("");
    jQuery('#ddl_howFound1').val("");
    jQuery('#txt_questions1').val("");
}

function shareWithusRequest() {

    var name = jQuery('#txt_firstName1').val();
    var emailid = jQuery('#txt_emailAddress1').val();
    var telephone = jQuery('#txt_telephone1').val();
    var reason = jQuery('#ddl_howFound1').val();
    var query = jQuery('#txt_questions1').val();

    var feedback = new Object();
    feedback.Name = name;
    feedback.FeedBack = query;
    feedback.Regarding = reason;
    feedback.EmailAddress = emailid;
    feedback.PhoneNumber = telephone;

    feedbackCallExecute(feedback);

    return false;

}

function feedbackCallExecute(feedbackModel) {
    if (null != feedbackModel) {
        jQuery('img.ajax-loader').show();
        jQuery.ajax({
            url: '/api/itinandhraapi/SendFeedback',
            type: 'POST',
            dataType: 'JSON',
            data: JSON.stringify(feedbackModel),
            contentType: "application/json",
            success: function (result) {

                if (result) {
                    alert("Thank you so much for contacting our organization. \n\n-ITinAP Team.\n");
                    jQuery('img.ajax-loader').hide();
                    clearcontactform();
                }
                else {
                    alert("Please submit it later, We are facing dificulties to take your feedback");
                    jQuery('img.ajax-loader').hide();
                }

            },
        });
    }
}

function apply() {

    var name = jQuery('#txt-name').val().trim();
    var emailid = jQuery('#txt-email').val().trim();
    var telephone = jQuery('#txt-phone').val().trim();
    var companyName = jQuery('#txt-companyname').val().trim();
    var category = jQuery('#txt-category').val().trim();
    var projectedEmp = jQuery("#projected-emp option:selected").val().trim();
    var projectedTurnover = jQuery("#projected-turnover option:selected").val().trim();
    var location = jQuery("#location option:selected").val().trim();
    var description = jQuery("#txt-description").val().trim();

    if (!name) {
        alert("Please provide your name.");
        jQuery('#txt-name').focus();
        return false;
    }

    if (!emailid) {
        alert("Please provide your email address.");
        jQuery('#txt-email').focus();
        return false;
    }

    if (!telephone) {
        alert("Please provide your mobile number.");
        jQuery('#txt-phone').focus();
        return false;
    }

    if (!category) {
        alert("Please provide a category.");
        jQuery('#txt-category').focus();
        return false;
    }

    if (!projectedEmp || projectedEmp == 0) {
        alert("Please choose projected employment in 2 years.");
        return false;
    }
    if (!projectedTurnover || projectedTurnover == 0) {
        alert("Please choose projected turnover");
        return false;
    }

    var apply = new Object();
    apply.Name = name;
    apply.EmailAddress = emailid;
    apply.MobileNumber = telephone;
    apply.CompanyName = companyName;
    apply.Category = category;
    apply.Employment = projectedEmp;
    apply.Turnover = projectedTurnover;
    apply.Location = location;
    apply.Description = description;

    companyApply(apply);

    return false;

}

function shareOnSocial(channel, pageurl, title) {
  //  alert("hui");
    if (pageurl == '') {
        pageurl = window.location.href;
    }
    var url;
    switch (channel) {
        case 'facebook':
            url = "http://www.facebook.com/sharer.php?u=" + pageurl + "&amp;t=" + title;
            window.open(url, '', 'width=320,height=400', '');
            break;
        case 'twitter':
            url = "http://twitter.com/share?url=" + pageurl + "&amp;text=" + title + "&amp;via=itinap";

            window.open(url, '', 'width=320,height=400', '');
            break;
        case 'gplus':
            url = "https://plus.google.com/share?url=" + pageurl;

            window.open(url, '', 'width=320,height=400', '');
            break;
        default:

    }
}

function companyApply(applyModel) {
    if (null != applyModel) {
        jQuery('img.ajax-loader').show();
        jQuery.ajax({
            url: '/api/itinandhraapi/InsertCompanyDetails',
            type: 'POST',
            dataType: 'JSON',
            data: JSON.stringify(applyModel),
            contentType: "application/json",
            success: function (result) {

                if (result) {
                    alert("Submitted successfully. \n\n-ITinAP Team.\n");
                    jQuery('img.ajax-loader').hide();
                    //clearcontactform();
                }
                else {
                    alert("Please submit it later, We are facing dificulties to submit your application.");
                    jQuery('img.ajax-loader').hide();
                }
            },
        });
    }
}

///Function to Check if any element is visible
try {
    jQuery.fn.is_on_screen = function () {
        var win = jQuery(window);

        var viewport = {
            top: win.scrollTop(),
            left: win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        var bounds = this.offset();
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
    };
}
catch (err)
{ }


var scrollTop;
jQuery(window).scroll(function () {
    scrollTop = jQuery(window).scrollTop();
    try {


        jQuery('img.lazy').each(function () {
            jQuerythis = jQuery(this);
            try {
                if (jQuerythis.is_on_screen() == true) {
                    jQuerythis.fadeOut(0, function () {
                        jQuerythis.attr('src', '');
                        jQuerythis.one("load", function () {
                        }).attr("src", jQuerythis.attr('data-original')).fadeIn(500);
                        jQuerythis.removeClass('lazy');
                    });
                }
            }
            catch (err) {
                //  console.log(err);
            }
        });


    if (jhomeSlogan.length && jhomeAdvantages.length) {
        if (jhomeAdvantages.hasClass('completed')) {
            trigger = jhomeAdvantages.offset().top - winHeight - 200;
            if (scrollTop <= 0 && actualWinWidth > 1024) {
                resetAdvantages();
                resetFeatures();
            }
        } else {
            trigger = jhomeSlogan.offset().top + jhomeSlogan.outerHeight() - winHeight;
            if (scrollTop >= trigger) {
                runAdvantages();
            }
        }
    }
    if (jhomeFeatures.length) {
        if (bodyWidth <= homeAdvantagesMobile) {
            jhomeFeatures.filter(':not(.completed)').each(function () {
                jfeature = jQuery(this);
                trigger = jfeature.offset().top - winHeight - 200 / 2;
                if (scrollTop >= trigger)
                    runFeatures(jfeature);
            });
        } else if (!jhomeFeatures.hasClass('completed')) {
            trigger = jhomeFeatures.first().offset().top - winHeight / 2;
            if (scrollTop >= trigger)
                runFeatures(jhomeFeatures);
        }
    }

    if (jQuery(this).scrollTop() > 200) {
        jQuery('#backtotop').fadeIn();
        //if (!isTweetsLoaded) {
            //getTweets();
            //isTweetsLoaded = true;
        //}
    }
    else {
        jQuery('#backtotop').fadeOut();
    }

    var homeLogos = jQuery("#parallax-div");
    if (scrollTop >= homeLogos.offset().top) {
        jQuery(".slider-div").css("opacity", "0");
       
    } else {
        jQuery(".slider-div").css("opacity", "1");
       
    }

    var scrollDownDiv = jQuery(".scroll-down");
    //var cityDiv = jQuery("#city-img");
    //cityDiv.css("top", "-" + scrollTop/2 + "px");

    var height = jQuery(".slider-div").outerHeight() + 80;

    

    if (scrollTop <= height) {
        //var backgroundIMG = jQuery("#bgDiv img");
        //backgroundIMG.css("top", "-" + scrollTop / 2 + "px");
        scrollDownDiv.css("bottom", scrollTop + "px");
       // jQuery("#home-logos").removeClass("fixed");
    }
    else {
      //  jQuery("#home-logos").addClass("fixed");
    }

    jQuery("#parallax-div").css("margin-top", height);
    jQuery("#slider-div").css({ "position": "fixed", "top": "80px" });
        
    } catch (e) {

    }


  
       // alert(isInnerPage);
        try {

            if (!isMobileDevice()) {
            if (scrollTop >= jQuery(".slider-div").outerHeight()) {

                jQuery("#home-logos").addClass("fixed");
                jQuery(".infra-benifit-gov").addClass("margintop80");
                //margintop80
            }
            else {
                jQuery("#home-logos").removeClass("fixed");
                jQuery(".infra-benifit-gov").removeClass("margintop80");
            }
            }

        } catch (e) {

        }
    

});

function isMobileDevice() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        return true;
        isFromApp = true;
    }
    else {
        return false;
        isFromApp = false;
    }
}

//For Man made Slider
// Determine user clicked outside popup or not
function DetermineUserClick(container, event) {
    // Closing popup when clicked out side
    if (!jQuery(container).is(event.target) // if the target of the click isn't the container...
            && jQuery(container).has(event.target).length === 0) // ... nor a descendant of the container
    {
        return true;
    }
    return false;
}


jQuery(document).ready(function () {

    jQuery('.toggleBar').click(function (e) {

        jQuery('html').toggleClass('noScroll');
        console.log("Click with menu has class: " + jQuery("#hp-side-menu").hasClass('active'));
        jQuery("#hp-side-menu,.hp-close-menu").toggleClass("active");

        if (jQuery("#hp-side-menu").hasClass('active')) {

            jQuery('.ja-body-container').addClass('menu-active');
            jQuery('#ja-menu-text').text('close');
            jQuery('#ja-menu-icon').removeClass('icon-align-justify').addClass('fa fa-times');
        }
        else {

            jQuery('.ja-body-container').removeClass('menu-active');
            jQuery('#ja-menu-text').text('menu');
            jQuery('#ja-menu-icon').removeClass('fa fa-times').addClass('icon-align-justify');
        }
    });

    jQuery(document).mouseup(function (e) {
        var id = jQuery(e.target).attr("data-id");
        if (id != "hp-sections-menu" && id != "show-section-menu" && DetermineUserClick("#hp-side-menu", e)) {
            jQuery("#hp-side-menu,.hp-close-menu").removeClass("active");
            jQuery("#hp-sections-menu").removeClass("selected");
            jQuery('.ja-body-container').removeClass('menu-active');
            jQuery('#ja-menu-text').text('menu');
            jQuery('#ja-menu-icon').removeClass('fa fa-times').addClass('icon-align-justify');
        }

        if (DetermineUserClick("#cbp-spmenu-shareIdea", e)) {
            jQuery('#cbp-spmenu-shareIdea').removeClass('cbp-spmenu-open');
        }
        if (DetermineUserClick("#cbp-spmenu-contact", e)) {
            jQuery('#cbp-spmenu-contact').removeClass('cbp-spmenu-open');
        }
    });

    jQuery(".hp-close-icon,.hp-close-menu").click(function () {
        alert("called")
        jQuery("#hp-search-bar,#hp-header,#hp-header-menubar,.hp-close-icon").removeClass("searchactive");
        jQuery("#hp-side-menu,.hp-close-menu").removeClass("active");
        jQuery("#hp-sections-menu,#hp-search-menu").removeClass("selected");
        jQuery('.ja-body-container').removeClass('menu-active');
    });

    jQuery('.toggleBar').keyup(function (e) {
        if (e.keyCode == 9) {
            jQuery('#toggleMenu').slideToggle();
            e.preventDefault();
            return false;
        }
    });

    jQuery('.close-menu').keyup(function (e) {
        if (e.keyCode == 9) {
            jQuery('#toggleMenu').slideUp();
            jQuery('.skip_to_main a').focus();
            e.preventDefault();
            return false;
        }
    });

    jQuery("#toggleMenu ul li a").each(function (index) {

        var blank_link = jQuery(this).attr('href');
        if (blank_link == '#') {
            jQuery(this).removeAttr("href").addClass('no-hover');
        }
    });
    //sm height
    var sm_height = jQuery('.info-wrapper').height();
    jQuery('.mn-height').height(sm_height);

    //

});

//<![CDATA[

if (!isDashboard) {
    jQuery(function () {
        jQuery('table').wrap('<div class="scroll-container"></div>');
    });
}
//]]>

//
jQuery(document).ready(function () {
    jQuery(".wwm_socialshare_imagewrapper .wwm_social_share li").live("click", function (e)
    { e.preventDefault(); execute_wwmfun(this); });
    jQuery(".gallery-share a").live("click", function (e) {
        e.preventDefault();
        var link, title, t, pageURL, imageUrl;
        t = jQuery(this);
        link = document.location.href;
        imageUrl = t.attr('href');
        title = t.attr('title');
        if (t.hasClass("wwm_facebook")) {
            pageURL = 'https://www.facebook.com/dialog/feed?app_id=1589276984617635&link=' + encodeURIComponent(link) + '&name=' + title + '&redirect_uri=' + encodeURIComponent(link) + '&picture=' + imageUrl + '';
        }
        if (t.hasClass("wwm_twitter")) {
            var id = t.attr('id');
            tweeterNewsImageShare(id);
            return;
        }
        if (t.hasClass("wwm_gplus")) {
            pageURL = 'https://plus.google.com/share?url=' + imageUrl + '';
        }
        if (t.hasClass("wwm_linked")) {
            pageURL = 'https://www.linkedin.com/shareArticle?mini=true&url=' + imageUrl + '';
        }
        PopupSocialMediaShare(pageURL, "Share", "580", "600");
        //PopupSocialMediaShare(pageURL, title, w, h)
    });

});

function PopupSocialMediaShare(pageURL, title, w, h) {
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    var targetWin = window.open(pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=yes, scrollbars=yes, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

function execute_wwmfun(e) {
    var t = jQuery(e).prarent().prev().attr("src");
    var n = "PM website";
    var r = jQuery(e).parent().prev().attr("alt");
    var i = "pmindia.gov.in";
    if (jQuery.trim(i) == "")
        var s = WWWM_FilterData(jQuery("title").text());
    else
        var s = i;
    var o = WWWM_FilterData(n);
    var u = jQuery(e).parent().prev("img").attr("src");
    var a = t;
    //document.location.href;
    var f = WWWM_FilterData(r);
    var l = ""; if (jQuery.trim(u) == "") u = t;
    if (jQuery.trim(n) == "") o = s;
    if (jQuery.trim(r) == "") r = s;
    if (jQuery(e).hasClass("wwm_facebook")) {
        wwm_fb_share(s, a, u, f, o)
    }
    if (jQuery(e).hasClass("wwm_twitter")) {
        var classNames = jQuery(e).parent().prev().attr('class').split(/\s+/), id;
        jQuery.each(classNames, function (index, item) {
            // Find class that starts with btn-
            if (item.indexOf("wp-image-") == 0) {                        // Store it
                iId = item.replace("wp-image-", "");
                tweeterNewsImageShare(iId);
            }
        });
    }
    if (jQuery(e).hasClass("wwm_gplus"))
    { var l = "https://plus.google.com/share?url=" + encodeURIComponent(a); wwm_common_share(l) }
    if (jQuery(e).hasClass("wwm_pinit"))
    { var l = "http://pinterest.com/pin/create/bookmarklet/?media=" + encodeURIComponent(u) + "&url=" + encodeURIComponent(a) + "& is_video=false&description=" + o; wwm_common_share(l) }
    if (jQuery(e).hasClass("wwm_tumblr"))
    { var l = "http://www.tumblr.com/share/photo?source=" + encodeURIComponent(u) + "&caption=" + o + "&clickthru=" + encodeURIComponent(a); wwm_common_share(l) }
    if (jQuery(e).hasClass("wwm_linked"))
    { var l = "http://www.linkedin.com/shareArticle?mini=true&url=" + encodeURIComponent(a) + "&title=" + s + "&source=" + encodeURIComponent(a); wwm_common_share(l) }
}
function WWWM_FilterData(e) {
    if (jQuery.trim(e) != "")
        return e.replace(/[^\w\sàâäôéèëêïîçùûüÿæœÀÂÄÔÉÈËÊÏÎŸÇÙÛÜÆŒ]/g, "");
    else return ""
}
function wwm_fb_share(e, t, n, r, i) {
    FB.ui({ method: "feed", name: e, link: t, picture: n, caption: r, description: i }, function (e) { if (e && e.post_id) { } else { } })
}
function wwm_common_share(e) { window.open(e, "", "menubar=no,toolbar=no,resizable=yes,scrollbars=no,height=400,width=600"); return false } jQuery(document).ready(function (e) {
    jQuery(".wwm_socialshare_imagewrapper").hover(function () {
        if (jQuery(this).find(".wwm_social_share").length == 0) {
            jQuery(this).find("img").after(jQuery(".wwm_social_share:eq(0)").clone())
        } jQuery(this).find(".wwm_social_share").show()
    }, function () { jQuery(".wwm_social_share").hide() }); var t = !!jQuery.fn.on; if (t) {
        jQuery(".wwm_socialshare_imagewrapper").on("click", ".wwm_social_share li", function (e) {
            e.preventDefault();
        })
    }
    else {
        jQuery(".wwm_socialshare_imagewrapper .wwm_social_share li").live("click", function (e)
        { e.preventDefault(); execute_wwmfun(this); }
          )
    }
})
